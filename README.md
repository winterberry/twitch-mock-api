# twitch-mock-api
Starts a mock server implementing the Twitch API based on `twitch-cli`.

More info on `twitch-cli`: <https://dev.twitch.tv/docs/cli>

Optionally, this container also runs a webhook through which commands can be received. It will be enabled if a valid configuration is found in `/hook/hook.yaml`.

The webhook is based on this program: <https://github.com/adnanh/webhook>

## How to run

To start the container:
```shell
docker run -d \
    -v $(pwd)/sample-hook:/hook \ # Mount the hook directory
    -p 8080:8080 \ # Listen on port 8080: Twitch API Server
    -p 9000:9000 \ # Listen on port 9000: Webhook Server
    twitch-mock-api
```

To call `verify-subscription` on the webhook (assuming the sample hook is loaded):
```shell
curl "http://localhost:9000/hooks/verify-subscription?forward-address=https://example.com&secret=test-secret"
```